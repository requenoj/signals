from pathlib import Path
import sys

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
from matplotlib.figure import Figure

def montre2csv(input_signal: str, output_signal: str) -> None:
    input_file = Path(input_signal).resolve()
    df_input_signal = pd.read_csv(input_file, sep="\s+", names=["duration", "signals"])
    df_output_signal = pd.DataFrame()
    df_output_signal["time"] = df_input_signal["duration"].cumsum()

    letters = set(df_input_signal["signals"])
    letters.remove("--")
    for l in letters:
        df_output_signal[l] = df_input_signal["signals"].apply(lambda x: l in x)

    # Dump to output file
    df_output_signal.to_csv(output_signal, sep=" ", header=True)


def plot_csv(input_signal: str, compact: bool = False) -> Figure:
    input_file = Path(input_signal).resolve()
    df_input_signal = pd.read_csv(input_file, sep="\s+")

    # Create one figure with num_subplots
    num_subplots: int = 1 if compact else len(df_input_signal.columns) - 1
    f, ax_list = plt.subplots(nrows=num_subplots, ncols=1, sharex=True)

    x_name = df_input_signal.columns[0]
    x = df_input_signal[x_name]

    for c in range(1, len(df_input_signal.columns)):
        y_name = df_input_signal.columns[c]
        y = df_input_signal[y_name]
        ax: Axes = ax_list[c - 1] if num_subplots > 1 else ax_list
        ax.step(x, y, where='post', label=y_name)
        ax.set_yticks([0, 1], minor=False)
        ax.legend()

    mini, maxi = df_input_signal[x_name].min(), df_input_signal[x_name].max()
    plt.xticks(range(mini, maxi))

    plt.show()
    return f


if __name__ == '__main__':
    output_signal = sys.argv[1]
    input_signal = sys.argv[2]
    montre2csv(input_signal, output_signal)
    plot_csv(output_signal, compact=False)