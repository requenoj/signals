import wfdb
import csv
import sys
import os


def get_ecg_from_file(ecg_filename):
    record = wfdb.rdsamp(ecg_filename)
    annotation = wfdb.rdann(ecg_filename, 'atr')
    # record = wfdb.rdsamp('mitdb/100', sampto=3000)
    # annotation = wfdb.rdann('mitdb/100', 'atr', sampto=3000)
    # wfdb.plotrec(record, annotation = annotation,
    #          title='Record 100 from MIT-BIH Arrhythmia Database',
    #          timeunits = 'seconds', figsize = (10,4), ecggrids = 'all')
    return record, annotation


def csv_converter(ecg_filename, csv_filename):
    ecg_record, ecg_annotation = get_ecg_from_file(ecg_filename)
    # signal_name_list = ['\"signal_' + str(i) + '\"' for i in range(len(ecg_record[0]))]
    # fieldnames = signal_name_list + ['\"time\"']
    signal_value_list = ecg_record[0]
    signal_name_list = ecg_record[1]['sig_name']
    fieldnames = ['\"time\"'] + signal_name_list

    outfile = open(csv_filename, 'wb')
    writer = csv.DictWriter(outfile, fieldnames=fieldnames, quoting=csv.QUOTE_MINIMAL, quotechar='\t')
    # writer.writeheader()

    # numsteps = ecg_record[1]['sig_len']  # Signal length
    # steps_size = 1.0 / ecg_record[1]['fs']  # Frequency
    # timestamp = 0.0
    timestamp = 0
    for signal_value_tuple in signal_value_list:
        d = {signal_name: signal_value for signal_name, signal_value in zip(signal_name_list, signal_value_tuple)}
        d[fieldnames[0]] = str(timestamp)  # + ' s'
        writer.writerow(d)
        # timestamp += steps_size
        timestamp += 1
    outfile.close()


if __name__ == '__main__':
    # ecg_filename = sys.argv[1]
    # csv_filename = sys.argv[2]
    ecg_filename, file_extension = os.path.splitext(sys.argv[1])
    csv_filename = ecg_filename + '.csv'

    csv_converter(ecg_filename, csv_filename)
