import sys
import numpy
from sympy import *
from multiprocessing import Pool, cpu_count
import csv


def eval_expr(expr, xpoint):
    # type: (_, tuple) -> float
    # xpoint == (2, 4, 0)
    var_list = expr.free_symbols
    # var_list == set([x])
    assert len(var_list) == len(xpoint)
    # list_var_val == [(x, 2), (y, 4), (z, 0)]
    list_var_val = zip(var_list, xpoint)
    expr_val = expr.subs(list_var_val)
    return float(expr_val)


def get_expr_from_file(eqfilename):
    # type: (str) -> list
    eqfile = open(eqfilename, 'r')
    # expre = sin(x) + (sin(2*x)/2) + (sin(3*x)/3) + (sin(4*x)/4)
    expre_list = [simplify(line) for line in eqfile]
    eqfile.close()
    return expre_list


def peval_expr(args):
    xpoint, signal, expre = args
    return xpoint[0], signal, eval_expr(expre, xpoint)


def csv_gen_par(eqfilename, numsteps, steps_size, outfilename):
    expre_list = get_expr_from_file(eqfilename)
    signal_name_list = ['\"signal_' + str(i) + '\"' for i in range(len(expre_list))]
    fieldnames = signal_name_list + ['\"time\"']


    outfile = open(outfilename, 'w')
    writer = csv.DictWriter(outfile, fieldnames=fieldnames, quoting=csv.QUOTE_MINIMAL, quotechar='\t')
    writer.writeheader()

    # Start multiprocessing
    num_proc = cpu_count()
    p = Pool(num_proc)

    k = numpy.arange(start=0, stop=numsteps, step=steps_size)
    args_peval_expr = (((timestamp, ), signal, expre) for timestamp in k for signal, expre in zip(signal_name_list, expre_list))
    #peval_res = p.map(peval_expr, args_peval_expr)
    peval_res = p.imap_unordered(peval_expr, args_peval_expr)

    # Stop multiprocessing
    p.close()
    p.join()

    temp = {timestamp: [] for timestamp in k}
    for item in peval_res:
        timestamp, signal, eval_res = item
        temp[timestamp] += [(signal, eval_res)]

    for timestamp in k:
        d = {signal: eval_res for signal, eval_res in temp[timestamp]}
        d[fieldnames[-1]] = str(timestamp) + ' s'
        writer.writerow(d)
    outfile.close()


def csv_gen(eqfilename, numsteps, steps_size, outfilename):
    expre_list = get_expr_from_file(eqfilename)
    signal_name_list = ['\"signal_' + str(i) + '\"' for i in range(len(expre_list))]
    fieldnames = signal_name_list + ['\"time\"']

    outfile = open(outfilename, 'w')
    writer = csv.DictWriter(outfile, fieldnames=fieldnames, quoting=csv.QUOTE_MINIMAL, quotechar='\t')
    writer.writeheader()

    for timestamp in numpy.arange(start=0, stop=numsteps, step=steps_size):
        xpoint = (timestamp,)
        #s = [eval_expr(expre, xpoint) for signal, expre in zip(signal_name_list, expre_list)]
        d = {signal: eval_expr(expre, xpoint) for signal, expre in zip(signal_name_list, expre_list)}
        d[fieldnames[-1]] = str(timestamp) + ' s'
        writer.writerow(d)
    outfile.close()

if __name__ == '__main__':
    # eqfilename = "/home/requenoj/Dropbox_Business/Dropbox/Recherche/pareto/STL/PyVCD/VCDGEN/eq_3.txt"
    # numsteps = 10
    # steps_size = 0.1
    # outfilename = "/home/requenoj/Dropbox_Business/Dropbox/Recherche/pareto/STL/PyVCD/VCDGEN/out_2.vcd"

    eqfilename = sys.argv[1]
    numsteps = int(sys.argv[2])
    steps_size = float(sys.argv[3])
    outfilename = sys.argv[4]

    #csv_gen(eqfilename, numsteps, steps_size, outfilename)
    csv_gen_par(eqfilename, numsteps, steps_size, outfilename)
