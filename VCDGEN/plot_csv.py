import sys
import pandas as pd
import plotly.express as px


def plot_csv(csvfile):
    df = pd.read_csv(csvfile)
    x = df.columns[1]  # signal_0
    y = df.columns[0]  # time
    fig = px.line(df, x = x, y = y)
    fig.show()

if __name__ == '__main__':
    # csvfile = "./VCDGEN/vcd/vcd4/signal.csv"

    csvfile = sys.argv[1]
    plot_csv(csvfile)
