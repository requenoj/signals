#!/bin/bash
. ~/.profile
for i in {1..5}
do
	python ./csv_gen_sympy.py ./vcd/vcd${i}/eq.txt 50 0.0001 ./vcd/vcd${i}/signal.csv
	#python ./vcd_gen_sympy.py ./vcd/vcd${i}/eq.txt 50 0.0001 ./vcd/vcd${i}/signal.vcd
	csv2vcd.real ./vcd/vcd${i}/signal.csv ./vcd/vcd${i}/signal.vcd &
done
