Triangle wave with amplitude a and period p using the modulo operation and absolute value is:

y(x)=4a/p |((x - p/4) mod p) - p/2| - a.

Triangle wave with amplitude=5, period=4
y(x)=5 |((x - 1) mod 4) - 2| - 5.
