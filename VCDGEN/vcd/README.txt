These folder contains several signals in VCD format.
In summary:
- vcd0: Original stabilization example
- vcd1: sin(t), cos(t)
- vcd2: Fourier serie approximation of a square wave
- vcd3: Fourier serie approximation of a triangle wave
- vcd4: Fourier serie approximation of a sawtooth wave
- vcd5: Damped sine wave

Fourier series equations are detailed in fourier_series.png file.
In general, period for Fourier series is:
(x0, x0 + 2L)

In particular, period these particular signals is:
L = pi
(x0, x0 + 2pi)

VCD files are created by sampling every signal with step 0.0001s during an interval of 50s.